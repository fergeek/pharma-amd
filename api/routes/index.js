const {Router} = require('express');
const route = Router();
const productsRoute = require('./products');
route.use('/products', productsRoute);
module.exports = route;
