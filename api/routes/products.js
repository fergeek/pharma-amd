const {Router} = require('express');
const Product = require('../models/Product');
const router = Router();
router.get('/:id/', (req, res) => {
  Product.matchById(parseInt(req.params.id)).then(product =>{
    let ok = typeof product === 'object';
    res.status(ok ? 200 : 206).send({success: ok, msg: ok ? 'success' : `ProductId: ${req.params.id} not found`, data: product || null});
  }).catch(err => {
    res.status(500).send({success: false, msg: err, data: null});
  });
});
router.get('/', (_ , res) => {
  Product.find({}).then(product => {
    res.status(200).send({success: true, msg: 'success', data: product});
  }).catch(err => {
    res.status(205).send({success: false, msg: err, data: null})
  });
});

router.put('/:id', (req, res) => {
    let attr = Product.groupAttr(req.body);
    Product.query().findById(parseInt(req.params.id)).patch(attr.attr).then(async p => {
      Object.keys(attr.relationals).forEach(async x => {
        let id = typeof attr.relationals[x] == 'number' ? attr.relationals[x] :  attr.relationals[x] instanceof String ? parseInt(attr.relationals[x]) : attr.relationals[x].id;
        // id = id instanceof Array ? id : [id];
        console.info(id, req.params.id);
        // Product.relatedQuery(x).for(p.id).relate(1).then(x=> console.log(x)).catch(err => console.log(err));
        await Product.relatedQuery(x).for(parseInt(req.params.id)).relate(id);
        // id.forEach(i => {Product.relatedQuery(x).for(p.id).relate(i);});
      });
      res.status(200).send({success: true, msg: 'success', data: p});
    }).catch(err => {
      res.status(206).send({success: false, msg: err, data: null});
    });
});
router.post('/', (req, res) => {
  let attr = Product.groupAttr(req.body);
  Product.insert(attr.attr).then(async p => {
    console.log(p);
    Object.keys(attr.relationals).forEach(async x => {
      let id = typeof attr.relationals[x] == 'number' ? attr.relationals[x] :  attr.relationals[x] instanceof String ? parseInt(attr.relationals[x]) : attr.relationals[x].id;
      id = id instanceof Array ? id : [id];
      id.forEach(async i => {await Product.relatedQuery(x).for(p.id).relate(i);});
    });
    res.status(200).send({success: true, msg: 'success', data: p});
  }).catch(err => {
        res.status(206).send({success: false, msg: err, data: null});
  });
});
module.exports = router;
