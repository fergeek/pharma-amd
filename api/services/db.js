const path = require('path')
const dbPath = path.resolve(__dirname, '../data/database.db')
const {Model} = require('objection');
const db = require('knex')({
  client: 'sqlite3',
  connection: {
    filename: dbPath,
  },
  useNullAsDefault: true
});
(async _ => {
  try {
    // db.schema
    await db.schema
    // products
    .createTableIfNotExists('products', table => {
      table.increment('id').primary();
      table.text('name');
      table.float('price');
      table.float('price');
      table.text('location');
      table.integer('date', 11);
      table.boolean('tracked');
      table.boolean('enabled').defaultTo(1);
      table.text('gtin');
      table.text('img');
      table.text('concentration');
    })
    // providers
    .createTableIfNotExists('providers', table => {
      table.increment('id').primary();
      table.text('name');
      table.text('address');
      table.text('phone');
      table.text('email');
    })
    // batch
    .createTableIfNotExists('batch', table => {
      table.increment('id').primary();
      table.integer('nro');
      table.expiration('expiration', 11);
      table.expiration('date', 11);
    })
    // manufacturer
    .createTableIfNotExists('manufacturer', table => {
      table.increment('id').primary();
      table.text('name');
    })
    // category
    .createTableIfNotExists('category', table => {
      table.increment('id').primary();
      table.text('name');
    })
    // presentation
    .createTableIfNotExists('presentation', table => {
      table.increment('id').primary();
      table.text('name');
    })
    /////////// TABLAS RELACION ///////////
    .createTableIfNotExists('batch_products_presentation', table => {
      table.integer('idProduct').references('products.id');
      table.integer('idBatch').references('batch.id');
      table.integer('idPresentation').references('presentation.id');
    })
    .createTableIfNotExists('products_manufacturer', table => {
      table.integer('idProduct').references('products.id');
      table.integer('idManufacturer').references('manufacturer.id');
    })
    .createTableIfNotExists('products_activePrinciple', table => {
      table.integer('idProduct').references('products.id');
      table.integer('activePrinciple').references('activePrinciple.id');
    })
    .createTableIfNotExists('products_categories', table => {
      table.integer('idProduct').references('products.id');
      table.integer('idProviders').references('providers.id');
    })
    .createTableIfNotExists('products_providers', table => {
      table.integer('idProduct').references('products.id');
      table.integer('idProviders').references('providers.id');
    })
    .createTableIfNotExists('providers_category', table => {
      table.integer('idProviders').references('providers.id');
      table.integer('idCategory').references('category.id');
    })
    //UTILS
    .createTableIfNotExists('sqlite_master', table => {
      table.text('name');
      table.text('type');
    });
    console.log('Database connected successfully.');

} catch(e){
  console.log(e);
}
});
Model.knex(db);
module.exports = {db, Model};
