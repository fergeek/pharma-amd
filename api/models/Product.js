const {Model} = require('../services/db');

class Product extends Model {
  static tableName = 'products';
  static get idColumn(){
    return 'id';
  }
  static insert(data){
    return this.query().insert(this.fixEntry(data));
  }
  static find(where={}){
    return this.query()
    .withGraphFetched('providers')
    .withGraphFetched('activePrinciples')
    .withGraphFetched('manufacturer')
    .withGraphFetched('category')
    .where(this.fixEntry(where));
  }
  static matchById(id){
    return this.query()
    .findById(id)
    .withGraphFetched('providers')
    .withGraphFetched('activePrinciples')
    .withGraphFetched('manufacturer')
    .withGraphFetched('category');
  }
  static update(where, query){
    return this.find(this.fixEntry(where)).patch(this.fixEntry(query));
  }
  static get relationMappings() {
    const ActivePrinciple = require('./ActivePrinciple');
    const Provider = require('./Provider');
    const Manufacturer = require('./Manufacturer');
    const Category = require('./Category');
    return {
      activePrinciples: {
        relation: Model.ManyToManyRelation,
        modelClass: ActivePrinciple,
        join: {
          from: 'products.id',
          through: {
            from: 'products_activePrinciple.idProduct',
            to: 'products_activePrinciple.idActivePrinciple'
          },
          to: 'activePrinciple.id'
        }
      },
      manufacturer: {
        relation: Model.ManyToManyRelation,
        modelClass: Manufacturer,
        join: {
          from: 'products.id',
          through: {
            from: 'products_manufacturer.idProduct',
            to: 'products_manufacturer.idManufacturer'
          },
          to: 'manufacturer.id'
        }
      },
      providers: {
        relation: Model.ManyToManyRelation,
        modelClass: Provider,
        join: {
          from: 'products.id',
          through: {
            from: 'products_providers.idProduct',
            to: 'products_providers.idProviders'
          },
          to: 'providers.id'
        }
      },
      category: {
        relation: Model.ManyToManyRelation,
        modelClass: Category,
        join: {
          from: 'products.id',
          through: {
            from: 'products_categories.idProducts',
            to: 'products_categories.idCategory'
          },
          to: 'category.id'
        }
      }
    };
  }
  static get jsonSchema() {
    return {
      type: 'object',
      // required: ['id'],
      properties:{
        id: {type: ['integer', 'null']},
        name: {type: 'text'},
        stock: {type: 'integer'},
        price: {type: 'real'},
        location: {type: 'text'},
        date: {type: 'integer'},
        tracked: {type: 'integer'},
        enabled: {type: 'integer'},
        gtin: {type: 'integer'},
        img: {type: 'text'},
        concentration: {type: 'text'},
      }
    };
  }
  static fixEntry(where, complement=false){
    let whereOut = {}, compl = {};
    Object.keys(where).forEach(x => {
      if(Object.keys(this.jsonSchema.properties).includes(x)){
        whereOut[x] = where[x];
      } else {
        compl[x] = where[x];
      }
    });
    return complement ? compl : whereOut;
  }
  static groupAttr(data){
    return {
      relationals: Object.fromEntries(Object.keys(this.fixEntry(data, true))
        .filter(x => Object.keys(this.relationMappings).includes(x))
        .map(x => [x, data[x]])),
      attr: this.fixEntry(data)
    };
  }
}
module.exports = Product;
