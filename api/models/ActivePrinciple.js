const {Model} = require('../services/db');

class ActivePrinciple extends Model {
  static tableName = 'activePrinciple';
  static find(where){
    return this.query().where(where);
  }
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name'],
      properties:{
        id: {type: ['integer', 'null']},
        name: {type: 'string'}
      }
    };
  }
};
module.exports = ActivePrinciple;
