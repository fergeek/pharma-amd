const {Model} = require('../services/db');

class Manufacturer extends Model {
  static tableName = 'manufacturer';
  static get jsonSchema() {
    return {
      type: 'object',
      // required: ['name'],
      properties:{
        id: {type: ['integer', 'null']},
        name: {type: 'string'}
      }
    };
  }
};
module.exports = Manufacturer;
