const {Model} = require('../services/db');

class Category extends Model {
  static tableName = 'category';
  static get jsonSchema() {
    return {
      type: 'object',
      // required: ['name'],
      properties:{
        id: {type: ['integer', 'null']},
        name: {type: 'string'}
      }
    };
  }
};
module.exports = Category;
